import { TextToSpeechPage } from './app.po';

describe('text-to-speech App', () => {
  let page: TextToSpeechPage;

  beforeEach(() => {
    page = new TextToSpeechPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
